FROM python:3.4

RUN useradd --create-home --shell /bin/bash user

WORKDIR /home/user/ordering-giosystem

COPY . .

RUN apt-get update \
    && apt-get install --yes \
        cdo \
        libldap2-dev \
        libsasl2-dev \
    && pip install --requirement requirements/dev.txt \
    && pip install --editable . \
    && apt-get purge -y \
        libldap2-dev \
        libsasl2-dev \
    && apt-get autoremove --yes \
    && install_pyxb_ogc_bindings -v \
    && mv scripts/docker-entrypoint /usr/local/bin/docker-entrypoint \
    && chmod 755 /usr/local/bin/docker-entrypoint

EXPOSE 8000

USER user

ENTRYPOINT ["docker-entrypoint"]
