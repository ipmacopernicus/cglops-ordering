"""Integration tests for oseoserver.operations.describeresultaccess"""

from lxml import etree

import pytest
from pyxb.bundles.opengis import oseo_1_0 as oseo

from oseoserver.operations import describeresultaccess
from oseoserver import models
from oseoserver import requestprocessor

pytestmark = pytest.mark.integration


@pytest.mark.parametrize("sub_function", [
    models.Batch.ALL_READY,
    models.Batch.NEXT_READY,
])
def test_describeresult_access(admin_user, sub_function):
    fake_url = "something"
    order = models.Order.objects.create(
        status=models.CustomizableItem.SUBMITTED,
        user=admin_user,
        order_type=models.Order.PRODUCT_ORDER,
        status_notification=models.Order.FINAL,
    )
    models.OrderDeliveryOption.objects.create(
        order=order,
        delivery_type="onlinedataaccess",
        delivery_details="ftp"
    )
    models.ItemSpecification.objects.create(
        order=order,
        collection="lst",
        item_id="first item"
    )
    batch = requestprocessor.create_product_order_batch(order)
    order_item = models.OrderItem.objects.get(batch=batch)
    order_item._set_status(
        models.CustomizableItem.COMPLETED,
        additional_info="Item processed with success"
    )
    order_item.url = fake_url
    order_item.expires_on = order_item._create_expiry_date()
    order_item.available = True
    order_item.save()
    request = oseo.DescribeResultAccess(
        service="OS",
        version="1.0.0",
        orderId=str(order.id),
        subFunction=sub_function
    )
    response = describeresultaccess.describe_result_access(
        request=request,
        user=admin_user
    )
    _print_response(response)
    url = response.URLs[0]
    assert url.itemId == order_item.item_specification.item_id
    assert url.itemAddress.ResourceAddress.URL == order_item.url


def _print_response(oseo_response):
    print(
        etree.tostring(
            etree.fromstring(
                oseo_response.toxml()
            ),
            pretty_print=True,
        ).decode("utf-8")
    )
