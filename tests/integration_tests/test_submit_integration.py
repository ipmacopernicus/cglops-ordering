"""Integration tests for oseoserver.operations.submit"""

import datetime as dt

import mock
import pytest
import pytz
from pyxb import BIND
from pyxb.bundles.opengis import oseo_1_0 as oseo
from pyxb.bundles.opengis import gml
import pyxb.binding.datatypes as xsd

from oseoserver.operations import submit
from oseoserver.models import Order

from core.xmlschema import cglops

pytestmark = pytest.mark.integration


@pytest.mark.django_db
def test_subscription_order_with_duration(settings, admin_user):
    start = dt.datetime(2017, 1, 1, tzinfo=pytz.utc)
    end = dt.datetime(2017, 1, 2, tzinfo=pytz.utc)
    request = oseo.Submit(
        service="OS",
        version="1.0.0",
        orderSpecification=oseo.OrderSpecification(
            orderType="SUBSCRIPTION_ORDER",
            option=[
                BIND(_build_date_range_option(start=start, end=end)),
            ],
            deliveryOptions=oseo.DeliveryOptionsType(
                onlineDataDelivery=BIND(
                    protocol=oseo.ProtocolType("ftp")
                )
            ),
            orderItem=[
                oseo.CommonOrderItemType(
                    itemId="some_item_id",
                    subscriptionId=oseo.SubscriptionIdType(
                        collectionId="urn:cgls:global:lst_v1_0.045degree"
                    )
                ),
            ]
        ),
        statusNotification="None"
    )
    response, order = submit.submit(request, admin_user)
    assert order.id is not None
    date_range = order.selected_options.get(option="DateRange")
    assert date_range.value == "start: {} stop: {}".format(
        start.isoformat(), end.isoformat())


@pytest.mark.django_db
def test_subscription_order_without_duration(settings, admin_user):
    request = oseo.Submit(
        service="OS",
        version="1.0.0",
        orderSpecification=oseo.OrderSpecification(
            orderType="SUBSCRIPTION_ORDER",
            deliveryOptions=oseo.DeliveryOptionsType(
                onlineDataDelivery=BIND(
                    protocol=oseo.ProtocolType("ftp")
                )
            ),
            orderItem=[
                oseo.CommonOrderItemType(
                    itemId="some_item_id",
                    subscriptionId=oseo.SubscriptionIdType(
                        collectionId="urn:cgls:global:lst_v1_0.045degree"
                    )
                ),
            ]
        ),
        statusNotification="None"
    )
    fake_start = dt.datetime(2016, 1, 1, tzinfo=pytz.utc)
    fake_end = fake_start + dt.timedelta(days=365)
    with mock.patch("oseoserver.operations.submit.dt",
                    autospec=True) as mock_dt:
        mock_dt.datetime.now.return_value = fake_start
        mock_dt.timedelta.return_value = dt.timedelta(days=365)
        response, order = submit.submit(request, admin_user)
    assert order.id is not None
    date_range = order.selected_options.get(option="DateRange")
    assert date_range.value == "start: {} stop: {}".format(
        fake_start.isoformat(), fake_end.isoformat())


@pytest.mark.django_db
@pytest.mark.parametrize("order_type", [
    Order.PRODUCT_ORDER,
    Order.SUBSCRIPTION_ORDER,
    Order.MASSIVE_ORDER,
])
@pytest.mark.parametrize("protocol", [
    "ftp",
    "http",
])
@pytest.mark.parametrize("delivery_type", [
    "onlinedataaccess",
    "onlinedatadelivery",
])
def test_order_with_delivery(settings, admin_user, order_type, protocol,
                             delivery_type):
    settings.OSEOSERVER_COLLECTIONS = [
        {
            "name": "lst",
            "catalogue_endpoint": "fake",
            "collection_identifier": "urn:cgls:global:lst_v1_0.045degree",
            "generation_Frequency": "Once per hour",
            order_type.lower(): {
                "enabled": True,
                "options": ["DateRange",],
                "online_data_access_options": [protocol,],
                "online_data_delivery_options": [protocol,],
            }
        },
    ]
    if order_type == Order.MASSIVE_ORDER:
        type_ = Order.PRODUCT_ORDER
        reference = Order.MASSIVE_ORDER_REFERENCE
        order_options = [
            BIND(_build_date_range_option(
                start=dt.datetime(2017, 1, 1),
                end=dt.datetime(2017, 2, 1)
            )),
        ]
    else:
        type_ = order_type
        reference = None
        order_options = None
    if delivery_type == "onlinedataaccess":
        delivery_options = oseo.DeliveryOptionsType(
            onlineDataAccess=BIND(
                protocol=oseo.ProtocolType(protocol)
            )
        )
    else:
        delivery_options = oseo.DeliveryOptionsType(
            onlineDataDelivery=BIND(
                protocol=oseo.ProtocolType(protocol)
            )
        )
    item = oseo.CommonOrderItemType(
        itemId = "some_item_id",
    )
    if order_type in (Order.PRODUCT_ORDER, Order.MASSIVE_ORDER):
        item.productId = oseo.ProductIdType(
            identifier="urn:cgls:global:lst_v1_0.045degree:LST_201010272200_"
                       "GLOBE_GEO_V1.2"
        )
    elif order_type == Order.SUBSCRIPTION_ORDER:
        item.subscriptionId = oseo.SubscriptionIdType(
            collectionId="urn:cgls:global:lst_v1_0.045degree"
        )

    request = oseo.Submit(
        service="OS",
        version="1.0.0",
        orderSpecification=oseo.OrderSpecification(
            orderReference=reference,
            orderType=type_,
            deliveryOptions=delivery_options,
            option=order_options,
            orderItem=[item]
        ),
        statusNotification="None"
    )
    response, order = submit.submit(request, admin_user)
    assert order.id is not None
    assert order.order_type == order_type
    assert order.selected_delivery_option.delivery_type == delivery_type
    assert order.selected_delivery_option.delivery_details == protocol


@pytest.mark.django_db
@pytest.mark.parametrize("upper_corner, lower_corner, expected", [
    ((1, 1), (0, 0), "ullon: 0.0 ullat: 1.0 lrlon: 1.0 lrlat: 0.0"),
    ((1, 2), (-1, -2), "ullon: -1.0 ullat: 2.0 lrlon: 1.0 lrlat: -2.0"),
])
@pytest.mark.parametrize("order_type", [
    Order.PRODUCT_ORDER,
    Order.SUBSCRIPTION_ORDER,
    Order.MASSIVE_ORDER,
])
def test_order_with_option_region_of_interest(settings, admin_user, order_type,
                                              upper_corner, lower_corner,
                                              expected):
    order_options = [
        BIND(_build_region_of_interest_option(
            upper_corner=upper_corner,
            lower_corner=lower_corner
        )),
    ]
    if order_type == Order.MASSIVE_ORDER:
        type_ = Order.PRODUCT_ORDER
        reference = Order.MASSIVE_ORDER_REFERENCE
        order_options.append(
            BIND(_build_date_range_option(
                start=dt.datetime(2017, 1, 1),
                end=dt.datetime(2017, 2, 1)
            )),
        )
    else:
        type_ = order_type
        reference = None
    item = oseo.CommonOrderItemType(
        itemId = "some_item_id",
    )
    if order_type in (Order.PRODUCT_ORDER, Order.MASSIVE_ORDER):
        item.productId = oseo.ProductIdType(
            identifier="urn:cgls:global:lst_v1_0.045degree:LST_201010272200_"
                       "GLOBE_GEO_V1.2"
        )
    elif order_type == Order.SUBSCRIPTION_ORDER:
        item.subscriptionId = oseo.SubscriptionIdType(
            collectionId="urn:cgls:global:lst_v1_0.045degree"
        )

    request = oseo.Submit(
        service="OS",
        version="1.0.0",
        orderSpecification=oseo.OrderSpecification(
            orderReference=reference,
            orderType=type_,
            option=order_options,
            deliveryOptions=oseo.DeliveryOptionsType(
                onlineDataAccess=BIND(
                    protocol=oseo.ProtocolType("ftp")
                )
            ),
            orderItem=[item]
        ),
        statusNotification="None"
    )
    response, order = submit.submit(request, admin_user)
    saved_option = order.selected_options.get(option="RegionOfInterest")
    assert saved_option.value == expected


def _build_date_range_option(start, end):
    parameter_data = oseo.ParameterData(
        encoding="XMLEncoding",
        values=xsd.anyType()
    )
    date_range = cglops.DateRange(
        "{start} {end}".format(
            start=start.strftime("%Y-%m-%dT%H:%M:%SZ"),
            end=end.strftime("%Y-%m-%dT%H:%M:%SZ"),
        )
    )
    parameter_data.values.append(date_range)
    return parameter_data


def _build_region_of_interest_option(upper_corner, lower_corner):
    parameter_data = oseo.ParameterData(
        encoding="XMLEncoding",
        values=xsd.anyType()
    )
    bbox = gml.boundingBox(
        upperCorner=" ".join(str(float(i)) for i in upper_corner),
        lowerCorner=" ".join(str(float(i)) for i in lower_corner)
    )
    region_of_interest = cglops.RegionOfInterest(bbox)
    parameter_data.values.append(region_of_interest)
    return parameter_data


def _print_response(oseo_response):
    print(
        etree.tostring(
            etree.fromstring(
                oseo_response.toxml()
            ),
            pretty_print=True,
        ).decode("utf-8")
    )
