"""Integration tests for oseoserver.operations.cancel"""

from lxml import etree

import pytest
from pyxb.bundles.opengis import oseo_1_0 as oseo

from oseoserver import errors
from oseoserver.operations import cancel
from oseoserver import models

pytestmark = pytest.mark.integration


def test_cancel_product_order(admin_user):
    order = models.Order.objects.create(
        status=models.CustomizableItem.SUBMITTED,
        user=admin_user,
        order_type=models.Order.PRODUCT_ORDER,
        status_notification=models.Order.FINAL,
    )
    request = oseo.Cancel(
        service="OS",
        version="1.0.0",
        orderId=str(order.id),
        statusNotification="None"
    )
    with pytest.raises(errors.ServerError):
        response = cancel.cancel(request=request, user=admin_user)
        _print_response(response)


@pytest.mark.parametrize("order_type", [
    models.Order.SUBSCRIPTION_ORDER,
    models.Order.MASSIVE_ORDER
])
def test_cancel_order(admin_user, order_type):
    order = models.Order.objects.create(
        status=models.CustomizableItem.SUBMITTED,
        user=admin_user,
        order_type=order_type,
        status_notification=models.Order.FINAL,
    )
    request = oseo.Cancel(
        service="OS",
        version="1.0.0",
        orderId=str(order.id),
        statusNotification="None"
    )
    response = cancel.cancel(request=request, user=admin_user)
    order.refresh_from_db()
    _print_response(response)
    response_element = etree.fromstring(response.toxml())
    assert etree.QName(response_element).localname == "CancelAck"
    assert order.status == models.Order.CANCELLED


def _print_response(oseo_response):
    print(
        etree.tostring(
            etree.fromstring(
                oseo_response.toxml()
            ),
            pretty_print=True,
        ).decode("utf-8")
    )
