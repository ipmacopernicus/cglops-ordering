"""Pytest configuration file for integration tests.

Simple utilities to facilitate integration tests.

"""

from functools import partial

from lxml import etree
import pytest
from pyxb import BIND
from pyxb.bundles.wssplat import soap12
from pyxb.bundles.wssplat import wsse


@pytest.fixture
def request_builder(request):
    return partial(
        _build_request,
        username=request.config.getoption("--request-user-name"),
        password=request.config.getoption("--request-user-password"),
        password_domain=request.config.getoption("--request-password-domain"),
    )


@pytest.fixture
def response_unwrapper():
    return _unwrap_response


def _build_request(oseo_element, username=None, password=None,
                   password_domain=None):
    """Builds an OSEO request, wrapped in a SOAP 1.2 envelope

    The proper SOAP header is also configured

    """

    security = wsse.Security(
        wsse.UsernameToken(
            username,
            wsse.Password(password)
        )
    )
    soap_request_env = soap12.Envelope(
        Header=BIND(security),
        Body=BIND(oseo_element)
    )
    envelope_element = etree.fromstring(
        soap_request_env.toxml(encoding="utf-8"))
    password_element = envelope_element.xpath(
        "ns1:Header/ns2:Security/ns2:UsernameToken/ns2:Password",
        namespaces=envelope_element.nsmap
    )[0]
    password_element.attrib["domain"] = password_domain
    serialized_request = etree.tostring(envelope_element, encoding="utf-8")
    return serialized_request, username, password, password_domain


def _unwrap_response(raw_response):
    soap_response_env = soap12.CreateFromDocument(raw_response)
    response_body = soap_response_env.Body.wildcardElements()[0]
    body_xml = response_body.toxml()
    body_element = etree.fromstring(body_xml)
    return body_element
