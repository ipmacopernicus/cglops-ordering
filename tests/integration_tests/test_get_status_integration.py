"""Integration tests for oseoserver.operations.submit"""

from lxml import etree

import pytest
from pyxb.bundles.opengis import oseo_1_0 as oseo

from oseoserver.operations import getstatus
from oseoserver import models
from oseoserver import requestprocessor

pytestmark = pytest.mark.integration


@pytest.mark.django_db
@pytest.mark.parametrize(
    "item_specification_identifiers, presentation, expected_items",
    [
        (["urn:cgls:global:lst_v1_0.045degree:LST_201606010000_GLOBE_GEO_V1.2"], "brief", 0),
        (["urn:cgls:global:lst_v1_0.045degree:LST_201606010000_GLOBE_GEO_V1.2"], "full", 1)
    ]
)
def test_get_status_order_id_items(admin_user, item_specification_identifiers,
                                   presentation, expected_items):
    order = models.Order.objects.create(
        status=models.CustomizableItem.SUBMITTED,
        user=admin_user,
        order_type=models.Order.PRODUCT_ORDER,
        status_notification=models.Order.FINAL,
    )
    for identifier in item_specification_identifiers:
        models.ItemSpecification.objects.create(
            order=order,
            remark="Some item remark",
            collection="lst",
            identifier = identifier,
            item_id="some id for the item",
        )
    requestprocessor.create_product_order_batch(order)
    request = oseo.GetStatus(
        service="OS",
        version="1.0.0",
        presentation=presentation,
        orderId=str(order.pk)
    )
    result = getstatus.get_status(request, admin_user)
    _print_response(result)
    assert isinstance(result, oseo.GetStatusResponseType)
    assert result.status == "success"
    monitor = result.orderMonitorSpecification[0]
    assert monitor.orderType == order.order_type
    assert monitor.orderId == str(order.pk)
    assert monitor.orderStatusInfo.status == order.status
    assert len(monitor.orderItem) == expected_items


def _print_response(oseo_response):
    print(
        etree.tostring(
            etree.fromstring(
                oseo_response.toxml()
            ),
            pretty_print=True,
        ).decode("utf-8")
    )
