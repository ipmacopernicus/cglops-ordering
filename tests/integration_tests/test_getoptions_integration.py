"""Integration tests for oseoserver.operations.getoptions"""

from lxml import etree
import pytest
from pyxb.bundles.opengis import oseo_1_0 as oseo

from oseoserver.operations import getoptions

pytestmark = pytest.mark.integration


def test_get_options_collection_id():
    request = oseo.GetOptions(
        service="OS",
        version="1.0.0",
        collectionId="urn:cgls:global:lst_v1_0.045degree"
    )
    response = getoptions.get_options(request=request, user=None)
    response_element = etree.fromstring(response.toxml(encoding="utf-8"))
    assert etree.QName(response_element).localname == "GetOptionsResponse"


def _print_response(oseo_response):
    print(
        etree.tostring(
            etree.fromstring(
                oseo_response.toxml()
            ),
            pretty_print=True,
        ).decode("utf-8")
    )
