"""Unit tests for core.orderprocessor"""

import datetime as dt

import mock
import pytest

from core import orderprocessor

pytestmark = pytest.mark.unit



@pytest.mark.parametrize("timeslot, collection, expected", [
    (
        dt.datetime(2016, 1, 1, 22, 0),
        "lst",
        "urn:cgls:global:lst_v1_0.045degree:LST_201601012200_GLOBE_GEO_V1.2"
    ),
(
        dt.datetime(2016, 1, 1, 22, 0),
        "lst10",
        "urn:cgls:global:lst10_v1_0.045degree:LST10_20160101_GLOBE_GEO_V1.0"
    ),
    (
        dt.datetime(2016, 1, 1, 22, 0),
        "lst10s",
        "urn:cgls:global:lst10s_v1_0.045degree:LST10S_20160101_GLOBE_GEO_V1.0"
    ),
])
def test_get_subscription_item_identifier(timeslot, collection, expected):
    processor = orderprocessor.OrderProcessor()
    result = processor.get_subscription_item_identifier(timeslot,
                                                        collection)
    assert result == expected


@pytest.mark.parametrize("start, end, expected", [
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 10),
        [
            dt.datetime(2017, 1, 1),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 11),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 12),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 21),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
            dt.datetime(2017, 1, 21),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 22),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
            dt.datetime(2017, 1, 21),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 2, 1),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
            dt.datetime(2017, 1, 21),
            dt.datetime(2017, 2, 1),
        ]
    ),
    (
        dt.datetime(2017, 1, 11), dt.datetime(2017, 1, 12),
        [
            dt.datetime(2017, 1, 11),
        ]
    ),
    (
        dt.datetime(2017, 1, 11), dt.datetime(2017, 1, 22),
        [
            dt.datetime(2017, 1, 11),
            dt.datetime(2017, 1, 21),
        ]
    ),
    (
        dt.datetime(2017, 1, 21), dt.datetime(2017, 1, 22),
        [
            dt.datetime(2017, 1, 21),
        ]
    ),
    (
        dt.datetime(2017, 1, 22), dt.datetime(2017, 3, 1),
        [
            dt.datetime(2017, 2, 1),
            dt.datetime(2017, 2, 11),
            dt.datetime(2017, 2, 21),
            dt.datetime(2017, 3, 1),
        ]
    ),
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 3, 1),
        [
            dt.datetime(2017, 1, 1),
            dt.datetime(2017, 1, 11),
            dt.datetime(2017, 1, 21),
            dt.datetime(2017, 2, 1),
            dt.datetime(2017, 2, 11),
            dt.datetime(2017, 2, 21),
            dt.datetime(2017, 3, 1),
        ]
    ),
])
def test_get_all_massive_order_identifiers_dekadal(start, end, expected):
    result = orderprocessor.get_all_massive_order_slots_dekadal(
        start, end)
    print("result: {}".format(result))
    assert result == expected


@pytest.mark.parametrize("start, end, expected", [
    (
        dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 2),
        [
            dt.datetime(2017, 1, 1, 0),
            dt.datetime(2017, 1, 1, 1),
            dt.datetime(2017, 1, 1, 2),
            dt.datetime(2017, 1, 1, 3),
            dt.datetime(2017, 1, 1, 4),
            dt.datetime(2017, 1, 1, 5),
            dt.datetime(2017, 1, 1, 6),
            dt.datetime(2017, 1, 1, 7),
            dt.datetime(2017, 1, 1, 8),
            dt.datetime(2017, 1, 1, 9),
            dt.datetime(2017, 1, 1, 10),
            dt.datetime(2017, 1, 1, 11),
            dt.datetime(2017, 1, 1, 12),
            dt.datetime(2017, 1, 1, 13),
            dt.datetime(2017, 1, 1, 14),
            dt.datetime(2017, 1, 1, 15),
            dt.datetime(2017, 1, 1, 16),
            dt.datetime(2017, 1, 1, 17),
            dt.datetime(2017, 1, 1, 18),
            dt.datetime(2017, 1, 1, 19),
            dt.datetime(2017, 1, 1, 20),
            dt.datetime(2017, 1, 1, 21),
            dt.datetime(2017, 1, 1, 22),
            dt.datetime(2017, 1, 1, 23),
            dt.datetime(2017, 1, 2, 0),
        ]
    ),
])
def test_get_all_massive_order_identifiers_hourly(start, end, expected):
    result = orderprocessor.get_all_massive_order_slots_hourly(
        start, end)
    assert result == expected


@pytest.mark.parametrize("collection, start, end, expected", [
    ("lst", dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 1, 5), [
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010000_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010100_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010200_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010300_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010400_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010500_GLOBE_GEO_V1.2",
    ]),
    ("lst10", dt.datetime(2017, 1, 1), dt.datetime(2017, 2, 1), [
        "urn:cgls:global:lst10_v1_0.045degree:LST10_20170101_GLOBE_GEO_V1.0",
        "urn:cgls:global:lst10_v1_0.045degree:LST10_20170111_GLOBE_GEO_V1.0",
        "urn:cgls:global:lst10_v1_0.045degree:LST10_20170121_GLOBE_GEO_V1.0",
        "urn:cgls:global:lst10_v1_0.045degree:LST10_20170201_GLOBE_GEO_V1.0",
    ]),
])
def test_get_all_massive_order_identifiers(collection, start, end, expected):
    processor = orderprocessor.OrderProcessor()
    result = processor.get_all_massive_order_identifiers(
        collection=collection, start=start, end=end)
    print("result: {}".format(result))
    assert result == expected


@pytest.mark.parametrize(
    "max_order_items, batch_index, collection, start, end, expected", [
    (3, 0, "lst", dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 1, 5), [
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010000_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010100_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010200_GLOBE_GEO_V1.2",
     ]),
    (3, 1, "lst", dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 1, 5), [
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010300_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010400_GLOBE_GEO_V1.2",
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010500_GLOBE_GEO_V1.2",
    ]),
    (3, 5, "lst", dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 1, 5), []),
    (3, -1, "lst", dt.datetime(2017, 1, 1), dt.datetime(2017, 1, 1, 5), []),
])
def test_get_massive_order_batch_identifiers(settings, max_order_items,
                                             batch_index, collection, start,
                                             end, expected):
    settings.OSEOSERVER_MAX_ORDER_ITEMS = max_order_items
    processor = orderprocessor.OrderProcessor()
    result = processor.get_massive_order_batch_item_identifiers(
        batch_index=batch_index, collection=collection, start=start, end=end)
    print("result: {}".format(result))
    assert result == expected


@pytest.mark.parametrize("bands, projection, format_", [
    (None, None, None),
    (["b1", "b2"], None, None),
    (None, "fake", None),
    (["b3"], None, "fake"),
])
def test_customize_item_no_roi(bands, projection, format_):
    path = "fake/path.nc"
    with mock.patch("core.orderprocessor.netcdfcustomizer",
                    autospec=True) as mock_customizer:
        result = orderprocessor.customize_item(
            path=path,
            bands=bands,
            region_of_interest=None,
            map_projection=projection,
            format_=format_
        )
        mock_customizer.customize_product.assert_called_once_with(
            path,
            variables=bands or [],
            region_of_interest=None
        )


@pytest.mark.parametrize("item_id, expected", [
    (
        "col_id:ITEM_201701011200_part",
        ("col_id", "item", dt.datetime(2017, 1, 1, 12))
    ),
    (
        "urn:cgls:global:lst_v1_0.045degree:LST_201701010300_GLOBE_GEO_V1.2",
        ("urn:cgls:global:lst_v1_0.045degree", "lst", dt.datetime(2017, 1, 1, 3))
    ),
])
def test_get_item_information(item_id, expected):
    result = orderprocessor.get_item_information(item_id)
    assert result == expected