"""pytest configuration file."""

import pytest


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "unit: run only unit tests"
    )
    config.addinivalue_line(
        "markers",
        "integration: run only integration tests"
    )


def pytest_addoption(parser):
    parser.addoption(
        "--request-user-name",
        default="dummy_user",
        help="Username to be used in integration tests"
    )
    parser.addoption(
        "--request-user-password",
        default="dummy_password",
        help="Password to be used in integration tests"
    )
    parser.addoption(
        "--request-password-domain",
        default="dummy_password_domain",
        help="Password domain to be used in integration tests"
    )


