# For now, this Dockerfile is to be used for development purposes only
# it is bundling the cglops-ordering and also the django-oseoserver projects
# together and instaling them in --editable mode

FROM python:3.4

ARG environment=dev

RUN useradd --create-home --shell /bin/bash user

WORKDIR /home/user/apps

COPY cglops_ordering cglops_ordering
COPY django-oseoserver django-oseoserver

RUN apt-get update \
    && apt-get install -y \
        cdo \
        libldap2-dev \
        libsasl2-dev \
    && cd django-oseoserver \
    && pip install --requirement requirements/${environment}.txt \
    && pip install --editable . \
    && cd ../ordering-giosystem \
    && pip install --requirement requirements/${environment}.txt \
    && pip install --editable . \
    && apt-get purge -y \
        libldap2-dev \
        libsasl2-dev \
    && apt-get autoremove -y \
    && install_pyxb_ogc_bindings -v

EXPOSE 8000

USER user

ENTRYPOINT ["gunicorn", "--workers=2", "--bind=0.0.0.0:8000", "--access-logfile", "-", "config.wsgi"]
