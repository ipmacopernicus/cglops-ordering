import pkg_resources

__version__ = pkg_resources.require("cglops_ordering")[0].version
