import datetime as dt
import logging
import os

from django.db import models
from django.conf import settings
import pytz

from oseoserver import models as oseoserver_models
from oseoserver import utilities

logger = logging.getLogger(__name__)


def upload_path_handler(instance, filename):
    processor = utilities.get_item_processor(
        instance.order_item.batch.order.order_type)
    collection, timeslot = processor.collection_manager.parse_item_identifier(
        instance.order_item.identifier)
    return (
        "{user}/order_{order_id:03d}/{collection}/{timeslot:%Y/%m/%d}/"
        "{filename}".format(
            user=instance.order_item.batch.order.user.username,
            order_id=instance.order_item.batch.order.id,
            collection=collection,
            timeslot=timeslot,
            filename=filename)
    )


def safe_rmdirs(dir_path):
    current_path = dir_path
    while current_path != settings.MEDIA_ROOT:
        try:
            os.rmdir(current_path)
        except (OSError, PermissionError, FileNotFoundError) as err:
            logger.warning(err)
            break
        current_path = current_path.rpartition(os.path.sep)[0]


class FileUpload(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    expires = models.DateTimeField(blank=True, null=True)
    data = models.FileField(upload_to=upload_path_handler)
    order_item = models.OneToOneField(
        oseoserver_models.OrderItem,
        on_delete=models.CASCADE,
    )

    def save(self, *args, **kwargs):
        if self.expires is None:
            self.expires = self._get_expiration_date()
        super(FileUpload, self).save(*args, **kwargs)

    # FIXME - This may not work with custom django storages
    def delete(self, using=None, keep_parents=False):
        """Reimplementing model.delete in order to delete the file.

        This method reimplements django's model.delete() in order to delete
        the uploaded files from the filesystem and also delete intermediary
        empty directories.

        """

        dir_path = os.path.join(settings.MEDIA_ROOT,
                                os.path.dirname(self.data.name))
        self.data.delete(save=False)
        safe_rmdirs(dir_path)
        super(FileUpload, self).delete(using=using, keep_parents=keep_parents)

    def _get_expiration_date(self):
        order_config = utilities.get_generic_order_config(
            self.order_item.batch.order.order_type)
        now = dt.datetime.now(pytz.utc)
        expiry_date = now + dt.timedelta(
            days=order_config.get("item_availability_days", 1))
        return expiry_date
