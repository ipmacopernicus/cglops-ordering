"""Functions for dealing with delivering and cleaning ordered items."""

import os
import logging
from urllib.parse import urlparse

import ftputil
from ftputil.error import PermanentError
import requests

logger = logging.getLogger(__name__)


def clean_item_online_data_access_http(url):
    """Delete an order item that has been made available at our HTTP service"""
    logger.debug("Cleaning item with url {!r}".format(url))
    response = requests.delete(url=url)
    response.raise_for_status()
    return response


def clean_item_online_data_access_ftp(url):
    """Delete an order item that has been made available at our FTP server."""
    parsed_url = urlparse(url)
    host = os.getenv("FTP_HOST", "")
    username = os.getenv("FTP_USER", "")
    password = os.getenv("FTP_PASSWORD", "")
    public_username = parsed_url.netloc.partition("@")[0]
    relative_path = parsed_url.path[1:]  # remove starting '/'
    full_path = os.path.join(
        os.getenv("FTP_PATH", ""),
        public_username,
        relative_path
    )
    with ftputil.FTPHost(host, username, password) as connection:
        connection.remove(full_path)
        _remove_ftp_empty_dirs(
            connection=connection,
            directory=os.path.dirname(full_path)
        )


def deliver_item_online_data_delivery(path, protocol, user,
                                      delivery_information):
    addresses = delivery_information["online_addresses"]
    locations = (info for info in addresses if info["protocol"] == protocol)
    urls = []
    if protocol == "ftp":
        for remote in locations:
            remote_user = remote["user_name"]
            host = remote["server_address"]
            remote_path = _deliver_ftp_push(
                item_path=path,
                host=host,
                remote_base_path=remote["path"],
                username=remote_user,
                password=remote["user_password"]
            )
            url = "ftp://{user}@{host}/{path}".format(
                user=remote_user, host=host, path=remote_path)
            urls.append(url)
    else:
        raise NotImplementedError("Protocol {!r} is not implemented for "
                                  "onlinedatadelivery".format(protocol))
    return urls


def deliver_item_online_data_access(path, protocol, user, order_id):
    handler = {
        "ftp": _deliver_item_online_data_access_ftp,
        "http": _deliver_item_online_data_access_http,
    }.get(protocol)
    try:
        url = handler(path=path, user=user, order_id=order_id)
    except TypeError:
        raise NotImplementedError("Protocol {!r} is not implemented for "
                                  "onlinedataaccess".format(protocol))
    return url


# TODO - finish this method
def _deliver_item_online_data_access_http(path, user, order_id):
    response = requests.post(
        url=None,
        data=None,
        headers={}
    )


def _deliver_item_online_data_access_ftp(path, user, order_id,
                                         ftp_remote_base_path=None,
                                         ftp_host=None,
                                         privileged_ftp_user=None,
                                         privileged_ftp_password=None):
    """Send the input path to the public FTP server.

    The ordered file is sent to the public FTP server via FTP by using a
    privileged account. The file is put in the relevant user's FTP area.

    Parameters
    ----------
    path: str
        The file path to send to the public FTP server
    user: str
        Username of the user that has ordered the file.
    order_id: int
        Id of the order that the file relates to.
    ftp_remote_base_path: str, optional
        Base path on the public FTP server that is used to store ordering
        requests. If this argument is not provided, the value of the
        ``FTP_PATH`` environment variable will be used.
    ftp_host: str, optional
        IP address or hostname of the public FTP server where the file is to
        be sent to. If this argument is not provided, the value of the
        ``FTP_HOST`` environment variable will be used.
    privileged_ftp_user: str, optional
        Username of the privileged account to use on the FTP server. This user
        account is used to send the file over FTP and put it into the correct
        location so that it becomes available to download by the end user. As
        such, the FTP server must be configured so that this user is not
        chrooted and is  able to write in any directories under the
        ``ftp_remote_base_path`` directory. If this argument is not provided,
        the value of the ``FTP_USER`` environment variable will be used.
    privileged_ftp_password: str, optional
        FTP password of the privileged user. If this argument is not provided,
        the value of the ``FTP_PASSWORD`` environment variable will be used.

    Returns
    -------
    url: str
        Public URL that the requesting user may access in order to pull the
        file

    """

    item_directory = os.path.join(
        ftp_remote_base_path or os.getenv("FTP_PATH", ""),
        user,
        "order_{:02d}".format(order_id),
    )
    host = ftp_host or os.getenv("FTP_HOST", "")
    remote_path = _deliver_ftp_push(
        item_path=path,
        host=host,
        remote_base_path=item_directory,
        username=privileged_ftp_user or os.getenv("FTP_USER", ""),
        password=privileged_ftp_password or os.getenv("FTP_PASSWORD", ""),
    )
    chrooted_path = remote_path.rsplit(user)[-1]
    url = "ftp://{user}@{host}{path}".format(
        user=user, host=host, path=chrooted_path)
    return url


def _deliver_ftp_push(item_path, host, remote_base_path="/",
                      username="anonymous", password=""):
    """Copy the input path to a remote server using FTP."""
    logger.info(
        "Pushing {!r} to {!r} through FTP...".format(item_path, host))
    remote_path = os.path.join(
        remote_base_path,
        os.path.basename(item_path)
    )
    with ftputil.FTPHost(host, username, password) as connection:
        # try to create the remote directory first, before uploading
        connection.makedirs(remote_base_path)
        connection.upload(item_path, remote_path)
    return remote_path


def _remove_ftp_empty_dirs(connection, directory):
    """Removes the input directory and their parents, if empty.

    Parameters
    ----------
    connection: ftputil.Connection
        The currently open FTP connection
    directory: str
        Remote directory that is to be deleted

    """

    while True:
        try:
            connection.rmdir(directory)
        except PermanentError:
            break
