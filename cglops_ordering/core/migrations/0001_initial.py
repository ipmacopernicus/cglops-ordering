# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-24 11:05
from __future__ import unicode_literals

from django.db import migrations
from django.conf import settings


def configure_site_instances(apps, schema_editor):
    SiteModel = apps.get_model("sites", "Site")
    #site_instance = SiteModel.objects.get(id=settings.SITE_ID)
    site_instance, created = SiteModel.objects.get_or_create(
        id=settings.SITE_ID,
        domain=settings.SITE_DOMAIN
    )
    #site_instance.domain = settings.SITE_DOMAIN
    #site_instance.save()


class Migration(migrations.Migration):

    dependencies = [
        ("sites", "__first__",)
    ]

    operations = [
        migrations.RunPython(configure_site_instances),
    ]
