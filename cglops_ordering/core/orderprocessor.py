"""A module to prepare orders of giosystem products."""
#
# * Sending files to be made available via onlinedataaccess with http protocol:
#
#   http -f POST localhost:8000/api/orderedfiles/ "Authorization: Token <the_token>" data@/path/to/file order_item=1
#
# * Deleting the previsouly sent file:
#
#   http DELETE localhost:8000/api/orderedfiles/<id> "Authorization: Token <the_token>"


from __future__ import absolute_import
from __future__ import division

from calendar import monthrange
import datetime as dt
from itertools import count
from itertools import product
from functools import lru_cache
import logging
from math import ceil
import os
import re
import shutil
import socket
import tempfile
from urllib.parse import urlparse
import zipfile

import dateutil.parser as dp
from jinja2.environment import Environment
import pysftp
from oseoserver import errors
import pkgutil
import yaml
from netcdf_cglops_tools import netcdfcustomizer

from . import deliverers

logger = logging.getLogger(__name__)



def customize_item(path, bands=None, region_of_interest=None,
                   map_projection=None, format_=None):
    """customize the order item.

    Parameters
    ----------
    path: str
        Path to the item that is to be customized
    bands: list, optional
        Names of the bands to extract from the item.
    region_of_interest: str
        Geographic bounding box for cropping the item
    map_projection: str
        Coordinate reference system to reproject the item into
    format_: str
        Alternative file format where the item should be saved

    Returns
    -------
    str
        The path to the customized file

    """

    parsed_bands = bands or []
    roi = None
    if region_of_interest:
        parts = region_of_interest.replace(":", "").split()
        roi_dict = dict()
        for i in range(0, len(parts) - 1, 2):
            roi_dict[parts[i]] = float(parts[i+1])
        roi = netcdfcustomizer.CdoLonLatBox(
            first_lon=roi_dict["ullon"],
            last_lon=roi_dict["lrlon"],
            first_lat=roi_dict["ullat"],
            last_lat=roi_dict["lrlat"],
        )
    if path.endswith(".nc"):
        result = netcdfcustomizer.customize_product(
            path,
            variables=parsed_bands,
            region_of_interest=roi,
        )
    else:
        raise IOError("Invalid path: {!r}".format(path))
    return result


def copy_robust(source, destination, force=True):
    if os.path.isdir(destination):
        output_path = os.path.join(destination, os.path.basename(source))
    else:
        output_path = destination
    if force:
        try:
            os.remove(output_path)
        except OSError:  # file is not there
            pass
    shutil.copy(source, output_path)


def makedirs_robust(destination):
    if not os.path.isdir(destination):
        os.makedirs(destination)


def modern_format(value, format_str):
    """A custom Jinja filter for formatting stuff.

    This is useful for allowing formats on datetime.datetime objects, as the
    default Jinja ``format`` filter does not work correctly.

    """

    try:
        result = format(value, format_str)
    except TypeError:
        result = value
    return result


def package_files(paths, order_id, output_dir):
    """Package files into a single file

    Parameters
    ----------
    paths: list
    The files to be packaged
    order_id: int
        Identifier of the order
    output_dir: str
        Directory where the zip will be created. It is assumed that this
        directory exists.

    Returns
    -------
    str
        The path to the newly created zip file

    """

    output_path = os.path.join(output_dir,
                               "order_{id}.zip".format(id=order_id))
    with zipfile.ZipFile(output_path, "w") as zip_handler:
        for path in paths:
            zip_handler.write(path, os.path.basename(path))
    return output_path


def estimate_item_size(num_variables, bounding_box=None):
    """Estimate the size of an individual item.

    Parameters
    ----------
    num_variables: int
        How many main variables does the item have
    bounding_box: netcdf_cglops_tools.netcdfanalyzer.BoundingBox, optional
        The geographic region of interest

    Returns
    -------
    float
        An estimation of the item's file size, in Mega Bytes.

    """

    # the following are a rough estimate
    ATTRIBUTES_SIZE_ESTIMATE_IN_MB = 0.1
    LAT_LON_OVERHEAD_IN_MB = 0.1
    SIZE_PER_SQUARED_DEGREE_PER_VARIABLE_IN_MB = 0.000026759

    if bounding_box is not None:
        longitude_span = (
            bounding_box.lower_right_lon - bounding_box.upper_left_lon)
        latitude_span = (
            bounding_box.upper_left_lat - bounding_box.lower_right_lat)
        area_of_interest_in_degrees = longitude_span * latitude_span
    else:
        area_of_interest_in_degrees = (180 - (-180)) * (80 - (-80))
    estimated_size = (
        SIZE_PER_SQUARED_DEGREE_PER_VARIABLE_IN_MB * num_variables * 
        area_of_interest_in_degrees +
        ATTRIBUTES_SIZE_ESTIMATE_IN_MB +
        LAT_LON_OVERHEAD_IN_MB
    )
    return estimated_size


def fetch_item(url, destination):
    """Fetch an item into destination

    This method supports the following URL schemes:

    * file - performs a local copy
    * sftp - uses pysftp to establish an SFTP connection and get the file

    Parameters
    ----------
    url: str
        The URL to fetch
    destination: str
        Path to the destination directory where the resource will be put.
        It is assumed that the destination directory already exists.

    Returns
    -------
    str
        The local path where the resource has been copied to

    Raises
    ------
    IOError
        When the resource cannot be fetched

    """

    parsed = urlparse(url)
    output_path = os.path.join(destination,
                               parsed.path.rpartition("/")[-1])
    if parsed.scheme == "file":
        copy_robust(parsed.path, output_path)
    elif parsed.scheme == "sftp":
        with pysftp.Connection(parsed.hostname, username=parsed.username,
                               password=parsed.password) as sftp, \
                pysftp.cd(destination):
            sftp.get(parsed.path)
    else:
        raise IOError(
            "Unsupported URL scheme: {!r}".format(parsed.scheme))
    return output_path


def get_all_massive_order_slots_hourly(start, end):
    slots = []
    for current_hour in count():
        slot = start + dt.timedelta(hours=current_hour)
        if slot <= end:
            slots.append(slot)
        else:
            break
    return slots


def get_all_massive_order_slots_dekadal(start, end):
    month_days = monthrange(start.year, start.month)[-1]
    count_base = start
    if start.day == 1:
        first_slot_day = 1
    elif start.day <= 11:
        first_slot_day = 11
    elif start.day <= 21:
        first_slot_day = 21
    else:  # advance to next month
        first_slot_day = 1
        count_base = (start + dt.timedelta(days=month_days)).replace(day=1)
    if first_slot_day == 1:
        slots = [count_base.replace(day=1)]
        if end.year == count_base.year and end.month == count_base.month:
            if end.day >= 11:
                slots.append(count_base.replace(day=11))
            if end.day >= 21:
                slots.append(count_base.replace(day=21))
        else:
            slots.append(count_base.replace(day=11))
            slots.append(count_base.replace(day=21))
    elif first_slot_day == 11:
        slots = [count_base.replace(day=11)]
        if end.year == count_base.year and end.month == count_base.month:
            if end.day >= 21:
                slots.append(count_base.replace(day=21))
        else:
            slots.append(count_base.replace(day=21))
    else:
        slots = [count_base.replace(day=21)]
    next_month = (count_base + dt.timedelta(days=month_days)).replace(day=1)
    while next_month <= end:
        current_month = next_month
        first = current_month.replace(day=1)
        second = current_month.replace(day=11)
        third = current_month.replace(day=21)
        if first <= end:
            slots.append(first)
        if second <= end:
            slots.append(second)
        if third <= end:
            slots.append(third)
        current_month_days = monthrange(
            current_month.year, current_month.day)[-1]
        next_month = (
            current_month + dt.timedelta(days=current_month_days)
        ).replace(day=1)
    return slots


@lru_cache()
def load_config(config_path):
    """Load configuration from the input path."""
    try:
        config = yaml.safe_load(
            pkgutil.get_data("core", config_path)
        )
    except FileNotFoundError:
        with open(config_path) as fh:
            config = yaml.safe_load(fh)
    return config


def parse_date_range_option(value):
    start, stop = [dp.parse(t) for t in value.text.split(" ")]
    result = "start: {} stop: {}".format(start.isoformat(),
                                         stop.isoformat())
    return result


def parse_region_of_interest_option(value):
    # although we coud use pyxb's gml bindings for parsing this option
    # we prefer not to do it. This way there is no need to make
    # pyxb a requirement for giosystem-ordering.
    nsmap= {"gml": "http://www.opengis.net/gml"}
    lower_corner = value.xpath(
        "gml:boundingBox/gml:lowerCorner/text()",
        namespaces=nsmap)[0].split()
    upper_corner = value.xpath(
        "gml:boundingBox/gml:upperCorner/text()",
        namespaces=nsmap)[0].split()
    try:
        crs = value.xpath("gml:boundingBox/@srsName", namespaces=nsmap)[0]
    except IndexError:
        crs = "EPSG:4326"
    lower_lon, lower_lat = _order_coordinates(lower_corner, crs)
    upper_lon, upper_lat = _order_coordinates(upper_corner, crs)
    logger.debug("lower_lon: {}".format(lower_lon))
    logger.debug("upper_lon: {}".format(upper_lon))
    left_lon, right_lon = (
        (lower_lon, upper_lon) if lower_lon < upper_lon else
        (upper_lon, lower_lon)
    )
    result = "ullon: {} ullat: {} lrlon: {} lrlat: {}".format(
        left_lon, upper_lat, right_lon, lower_lat)
    logger.debug("result: {}".format(result))
    return result


def _order_coordinates(coordinate_pair, crs):
    code = crs.rpartition(":")[::2]
    if code == "4326":
        y, x = coordinate_pair
    elif code == "CRS84":
        x, y = coordinate_pair
    else:  # fallback to Y X
        y, x = coordinate_pair
    return x, y


class CollectionManager(object):

    def __init__(self, config_path=None, data_roots=None,
                 jinja_environment=None):
        self.config_path = config_path or os.getenv("COLLECTION_CONFIG_PATH",
                                                    "collections-config.yml")
        self.data_roots = data_roots or os.getenv("DATA_ROOTS", "").split(":")
        self.jinja_environment = jinja_environment or Environment()
        self.jinja_environment.filters["format"] = modern_format

    def get_catalogue_identifier(self, collection, **kwargs):
        return "{collection_identifier}:{dataset_identifier}".format(
            collection_identifier=self.get_collection_identifier(
                collection=collection,
            ),
            dataset_identifier=self._get_dataset_identifier(
                collection=collection,
                **kwargs
            )
        )

    def get_collection_identifier(self, collection):
        return (
            "urn:cgls:global:{name}_v1_0.045degree".format(name=collection))

    def get_file_pattern(self, collection, **kwargs):
        config = self._get_collection(collection)
        template = self.jinja_environment.from_string(config["file_pattern"])
        return template.render(
            dataset_identifier=self._get_dataset_identifier(
                collection, **kwargs),
            **kwargs
        )

    def get_generation_frequency(self, collection):
        config = self._get_collection(collection)
        return config["generation_frequency"]

    def get_search_urls(self, collection, **kwargs):
        config = self._get_collection(collection)
        url_patterns = config["search_urls"]
        urls = []
        for search_url, root in product(url_patterns, self.data_roots):
            template = self.jinja_environment.from_string(search_url)
            urls.append(
                template.render(
                    data_root=root,
                    name=collection,
                    file_pattern=self.get_file_pattern(collection, **kwargs),
                    **kwargs
                )
            )
        return urls

    def _get_collection(self, name):
        config = load_config(config_path=self.config_path)
        return config[name]

    def _get_dataset_identifier(self, collection, **kwargs):
        config = self._get_collection(collection)
        template = self.jinja_environment.from_string(
            config.get("dataset_identifier"))
        return template.render(
            name=collection,
            **kwargs
        )

    @classmethod
    def parse_item_identifier(cls, identifier):
        """Retrieve an order item's parameters from its catalogue identifier.

        Parameters
        ----------
        identifier: str
            The item's catalogue identifier

        Returns
        -------
        collection_name: str
            The name of the item's collection
        timeslot: datetime.datetime
            The timeslot of the item

        """

        collection_id, dataset_id = identifier.rpartition(":")[::2]
        collection_name = collection_id.rpartition(":")[-1].partition("_")[0]
        timeslot_str = dataset_id.split("_")[1]
        timeslot = dp.parse(timeslot_str)
        return collection_name, timeslot


class FtpPullPreparator(object):
    LOCAL_ROOT = "/home/ftpuser"

    def deliver(self, path, username, order_id, **kwargs):
        """Copy the input path to an FTP accessible location on the local host.

        The input path will become available at a local FTP server, where it
        can be downloaded by clients.

        Parameters
        ----------
        path: str
            Path of the file to be delivered via FTP pull
        username: str
            User that made the request
        order_id: int
            Order that relates to the request

        Returns
        -------
        url: str
            URL where the file may be accessed
        output_path: str
            Path to the file in the local filesystem

        """

        output_dir = os.path.join(self.LOCAL_ROOT, username,
                                  "order_{id}".format(id=order_id))
        output_path = os.path.join(output_dir, os.path.basename(path))
        makedirs_robust(output_dir)
        copy_robust(path, output_path)
        hostname = socket.getfqdn()
        pattern = "ftp://{user}@{host}:{path}"
        relative_path = output_path.split(username)[-1]
        url = pattern.format(user=username, host=hostname,
                             path=relative_path)
        return url, output_path

    def get_file_path(self, url):
        """Get the path on the local filesystem

        Parameters
        ----------
        url: str
            URL where the file is accessible to external clients

        Returns
        -------
        file_path: str
            Path to the file

        """

        parsed_url = urlparse(url)
        if parsed_url.scheme == "ftp":
            file_path = os.path.join(self.LOCAL_ROOT, parsed_url.username,
                                     parsed_url.path.lstrip("/"))
        else:
            raise errors.OseoServerError(
                "invalid protocol: {!r}".format(parsed_url.scheme))
        return file_path


class HttpPullPreparator(object):
    LOCAL_ROOT = "/home/ftpuser"

    def deliver(self, path, username, order_id, item_id=None):
        """Copy input path to an HTTP accessible location on the local host.

        The input path will become available at a local HTTP server, where
        it can be downloaded by clients.

        Parameters
        ----------
        path: str
            Path of the file to be delivered via FTP pull
        username: str
            User that made the request
        order_id: int
            Order that relates to the request
        item_id: int, optional
            Identifier of the order item that relates to the request

        Returns
        -------
        url: str
            URL where the file may be accessed
        output_path: str
            Path to the file in the local filesystem

        """

        output_dir = os.path.join(self.LOCAL_ROOT, username,
                                  "order_{id}".format(id=order_id))
        output_path = os.path.join(output_dir, os.path.basename(path))
        makedirs_robust(output_dir)
        copy_robust(path, output_path)
        hostname = socket.getfqdn()

        filename = os.path.basename(output_path)
        pattern = (
            "http://{host}/{sub_uri}{user}/order_{id}{itemid}/{name}")
        item_id_path = "/{id}".format(id=item_id) if item_id else ""
        url = pattern.format(
            host=hostname,
            sub_uri=os.getenv("GIOSYSTEM_DOWNLOADS_SUB_URI", ""),
            user=username,
            id=order_id,
            itemid=item_id_path,
            name=filename,
        )
        return url, output_path

    def get_file_path(self, url):
        """Get the path on the local filesystem

        Parameters
        ----------
        url: str
            URL where the file is accessible to external clients

        Returns
        -------
        file_path: str
            Path to the file

        """

        parsed_url = urlparse(url)
        if parsed_url.scheme == "http":
            path_parts = parsed_url.path.lstrip("/").split("/")
            if "item_" in parsed_url.path:
                username, order_str, item_str, filename = path_parts[-4:]
            else:
                username, order_str, filename = path_parts[-3:]
            file_path = os.path.join(self.LOCAL_ROOT, username, order_str,
                                     filename)
        else:
            raise errors.OseoServerError(
                "invalid protocol: {!r}".format(parsed_url.scheme))
        return file_path


class OrderProcessor(object):
    _working_dir = None

    def __init__(self):
        self.collection_manager = CollectionManager()

    def cleanup(self):
        """Remove any temporary directories and their contents.

        This method is called by an instance of
        ``oseoserver.models.OrderItem`` after an item has been processed and
        delivered. It provides a hook for cleaning up any temporary resources
        that might have been created during the deliver and process stage.
        """
        if self._working_dir is not None:
            logger.debug("Cleaning up...")
            try:
                shutil.rmtree(self._working_dir)
            except Exception as err:
                logger.info(err)

    def clean_item(self, url):
        """Clean an item that was previously available via 'onlinedataaccess'.

        Cleaning of order items means that they should not be available
        anymore. Depending on the url, this may mean different things. For
        example, if the url specifies an ftp scheme, the items may be deleted
        from the ftp server.

        This method is called by oseoserver's models.OrderItem.expire() method
        and only applies to items that specify 'onlinedataaccess' as their
        delivery type.

        Parameters
        ----------
        url: str
            Public URL of the item to be deleted

        """

        parsed_url = urlparse(url)
        handler = {
            "http": deliverers.clean_item_online_data_access_http,
            "ftp": deliverers.clean_item_online_data_access_ftp,
        }.get(parsed_url.scheme)
        return handler(url)

    def estimate_number_massive_order_items(self, collection, start, end):
        """Estimate the number of items that are part of an order."""
        return len(
            self.get_all_massive_order_identifiers(
                collection=collection, start=start, end=end)
        )

    def estimate_number_massive_order_batches(self, collection, start, end,
                                              items_per_batch):
        identifiers = self.get_all_massive_order_identifiers(
            collection=collection, start=start, end=end)
        result = ceil(len(identifiers) / items_per_batch)
        return result

    def get_collection_id(self, item_id):
        """Determine the collection identifier for the specified item.

        This method is used when the requested order item does not provide the
        optional 'collectionId' element.

        The current implementation relies on the standard naming conventions
        for collections and products in use by the CGLOPS partners. This
        naming convention states that for each individual product, its
        identifier shall be of the form:

            <collection_identifier>:<item_specific_identifier>

        As such it is not necessary to query the catalogue server in order to
        determine the collection identifier for any given order item

        Parameters
        ----------
        item_id: str
            Identifier of an order item that belongs to the collection to
            find

        Returns
        -------
        str
            Identifier of the collection
        """

        collection, timeslot = self.collection_manager.parse_item_identifier(
            item_id)
        return self.collection_manager.get_collection_identifier(collection)

    def get_all_massive_order_identifiers(self, collection ,start, end):
        frequency = self.collection_manager.get_generation_frequency(
            collection)
        slots = []
        if frequency == "hourly":
            slots = get_all_massive_order_slots_hourly(start, end)
        elif frequency == "dekadal":
            slots = get_all_massive_order_slots_dekadal(start, end)
        identifiers = [
            self.get_item_catalogue_identifier(s, collection) for s in slots]
        return identifiers

    def get_item_catalogue_identifier(self, timeslot, collection):
        """Find the catalogue identifier for an item.

        Parameters
        ----------
        timeslot: datetime.datetime
            The temporal date and time for the subscription batch to be
            processed
        collection: str
            Name of the collection

        Returns
        -------
        identifier: str
            The catalogue identifier of the relevant order item

        """
        return self.collection_manager.get_catalogue_identifier(
            collection=collection,
            timeslot=timeslot
        )

    def get_massive_order_batch_item_identifiers(self, batch_index,
                                                 collection, start, end,
                                                 items_per_batch):
        """Find catalogue identifiers for items of a massive order batch"""
        all_identifiers = self.get_all_massive_order_identifiers(
            collection=collection, start=start, end=end)
        start_index = batch_index * items_per_batch
        batch_items = all_identifiers[start_index:start_index+items_per_batch]
        return batch_items

    def get_order_duration(self, item_specification):
        date_range = ""
        try:
            date_range = item_specification.selected_options.filter(
                option="DateRange")[0].value
        except IndexError:
            date_range = item_specification.order.selected_options.filter(
                option="DateRange")[0].value
        date_pattern = r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[+-]\d{2}:\d{2}"
        pattern = "start: (?P<start>{0}) stop: (?P<end>{0})".format(
            date_pattern)
        re_obj = re.search(pattern, date_range)
        if re_obj is not None:
            start = dp.parse(re_obj.groupdict()["start"])
            end = dp.parse(re_obj.groupdict()["end"])
        else:
            start = None
            end = None
        return start, end

    def get_subscription_item_identifier(self, timeslot, collection):
        """Find the catalogue identifier for item of a subscription batch.

        Parameters
        ----------
        timeslot: datetime.datetime
            The temporal date and time for the subscription batch to be
            processed
        collection: str
            Name of the collection

        Returns
        -------
        identifier: str
            The catalogue identifier of the relevant order item

        """
        return self.get_item_catalogue_identifier(timeslot, collection)

    @classmethod
    def parse_option(cls, name, value, **kwargs):
        """
        Parse the input option into a string for storing in the order database

        :param name:
        :param value:
        :param kwargs:
        :return:
        """

        if name == "RegionOfInterest":
            result = parse_region_of_interest_option(value)
        elif name == "DateRange":
            result = parse_date_range_option(value)
        else:
            result = value.text
        return result

    @staticmethod
    def parse_extension(extension_text):
        """Parse an extension to OSEO"""

        name, value = [i.strip() for i in extension_text.split(":")]
        return name, value

    def prepare_item(self, identifier, working_dir=None, options=None):
        """Prepare an order item according to the input options

        This method is called by an ``oseoserver.models.OrderItem`` instance
        in its ``process()`` method.

        Parameters
        ----------
        identifier: str
            The item's catalogue identifier
        working_dir: str, optional
            A directory where the prepared item and any temporary files will
            be placed. The caller is responsible for cleaning this directory
            after the item has been dealt with.
        options: dict, optional
            A mapping with the customization options that are to be applied
            to the item. Accepted keys and values are:

            * Bands: A space separated string with the names of the product
              variables that are to be extracted into a new output file

            * RegionOfInterest: A tuple with the region of interest to use
              for cropping the product

            * MapProjection: A string with the proj4 definition of a
              projection to use to repoject the original data

            * Format: A string with the name of another file format to
              convert data into.

        Returns
        -------
        str
            A url to the prepared item

        Raises
        ------
        oseoserver.errors.OseoServerError
            If the item cannot be found

        """

        collection, timeslot = self.collection_manager.parse_item_identifier(
            identifier)
        self._working_dir = working_dir or tempfile.mkdtemp()
        urls = self.collection_manager.get_search_urls(
            collection=collection,
            timeslot=timeslot,
        )
        for search_url in urls:
            logger.debug("searching in {!r} ...".format(search_url))
            fetched = fetch_item(search_url, self._working_dir)
            if options is not None:
                result = customize_item(
                    fetched,
                    bands=options.get("Bands", "").split(),
                    region_of_interest=options.get("RegionOfInterest"),
                    map_projection=options.get("MapProjection"),
                    format_=options.get("Format")
                )
            else:
                result = fetched
            if result:
                break
        else:
            raise errors.OseoServerError("Could not find the requested "
                                         "item: {!r}".format(identifier))
        return urlparse(result, scheme="file").geturl()

    def deliver_item(self, item_url, user_name, delivery_options,
                     identifier=None, order_id=None, batch_id=None,
                     item_id=None, packaging=False, delivery_information=None):
        """Deliver a single order item.

        Parameters
        ----------
        item_url: str
            Local URL where the already prepared item is accessible
        user_name: str
            The username of the user that ordered the item
        delivery_options: dict
            A mapping with the delivery options set on the item. Accepted keys
            and values are:

            * delivery_type: The type of delivery to perform. Must be one of
              oseoserver.models.BaseDeliveryOption.ONLINE_DATA_ACCESS or
              oseoserver.models.BaseDeliveryOption.ONLINE_DATA_DELIVERY

            * protocol: The protocol to use for delivery must be one of
              'http', 'ftp'

            * special_instructions: A string with additional textual
              information about the  delivery. NOT CURRENTLY IMPLEMENTED.

            * copies: An int specifying how many copies of the output files
              should be generated. NOT CURRENTLY IMPLEMENTED.

            * annotation: A string with additional textual information about
              the delivery. NOT CURRENTLY IMPLEMENTED.

        Returns
        -------
        url: str
            URL where the file may be accessed by the user that placed the
            order

        Other Parameters
        ----------------
        identifier: str, optional
            The order item's identifier in the CSW catalogue
        order_id: int, optional
            The id of the order, as stored in oseoserver's database
        batch_id: int, optional
            The id of the batch, as stored in oseoserver's database
        item_id: str, optional
            The id assigned by the user when the order item was requested

        """

        parsed_url = urlparse(item_url)
        item_path = parsed_url.path
        if delivery_options["delivery_type"] == "onlinedatadelivery":
            urls = deliverers.deliver_item_online_data_delivery(
                path=item_path,
                protocol=delivery_options["protocol"],
                user=user_name,
                delivery_information=delivery_information
            )
            url = " ".join(urls)
        elif delivery_options["delivery_type"] == "onlinedataaccess":
            url = deliverers.deliver_item_online_data_access(
                path=item_path,
                protocol=delivery_options["protocol"],
                user=user_name,
                order_id=order_id,
            )
        else:  # media delivery
            raise NotImplementedError
        return url
