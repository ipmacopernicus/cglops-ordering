"""Custom middlewares for ordering-giosystem"""

import logging

from django.contrib.auth import authenticate
from lxml import etree

from oseoserver import soap

logger = logging.getLogger(__name__)


class VitoAuthenticationMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            request_element = etree.fromstring(request.body)
            details = soap.unwrap_request(request_element)
            request_data, username, password, password_attributes = details
            user = authenticate(username=username, password=password,
                                password_attributes=password_attributes)
        except etree.XMLSyntaxError:
            logger.debug("Could not authenticate user with {0!r}".format(
                self.__class__.__name__))
        else:
            request.user = user
        finally:
            response = self.get_response(request)
            return response
