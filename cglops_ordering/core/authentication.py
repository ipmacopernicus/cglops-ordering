# Copyright 2016 Ricardo Garcia Silva
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""Authentication for ordering-giosystem

This functionality is implemented as a separate module in order to allow
different authentication methods to be used by the oseoserver.server.OseoServer
class.

"""

import logging
import os

from django.contrib.auth import get_user_model
from django.conf import settings
import ldap
from oseoserver import errors
#from rest_framework import authentication

logger = logging.getLogger(__name__)


def get_user_info(username, timeout_seconds=5):
    """Retrieve user information from VITO's LDAP server"""

    server_url = settings.AUTH_LDAP_SERVER_URI
    dn = settings.AUTH_LDAP_BIND_DN
    password = settings.AUTH_LDAP_BIND_PASSWORD
    base_dn = settings.AUTH_LDAP_USER_SEARCH.base_dn
    if not (server_url and dn and base_dn):
        raise errors.OseoServerError("Authentication server not defined.")
    connection = ldap.initialize(server_url)
    connection.set_option(ldap.OPT_TIMEOUT, timeout_seconds)
    connection.set_option(ldap.OPT_NETWORK_TIMEOUT, timeout_seconds)
    try:
        connection.bind_s(dn, password, ldap.AUTH_SIMPLE)
        info = connection.search_s(base_dn,
                                   ldap.SCOPE_SUBTREE,
                                   "(uid={})".format(username))
        result = info[0][1]
    except (ldap.LDAPError, ldap.SERVER_DOWN) as err:
        raise errors.OseoServerError(str(err))
    except IndexError:
        raise errors.AuthenticationFailedError()
    else:
        return result
    finally:
        connection.unbind()


class VitoBackend(object):
    """Custom django authentication backend to be used in pyoseo.

    For more information on django authentication backends visit:

    https://docs.djangoproject.com/en/1.9/topics/auth/customizing/#writing-an-authentication-backend

    Users are authenticated against VITO's remote LDAP server. Each user
    that is successfully authenticated has a local django user created and
    assigned to it.

    VITO sensitive data such as LDAP credentials is loaded from the
    environment. The following env vars are expected:

    * VITO_SOAP_PASSWORD - The previously agreed password token for OSEO SOAP
      requests made by VITO's ordering service
    * VITO_SOAP_PASSWORD_TYPE - The previously agreed password type attribute
      for OSEO SOAP requests made by VITO's ordering service
    * VITO_LDAP_SERVER - URL of the LDAP server that will be contacted in order
      to verify if a user has registered with VITO's online Copernicus GL
      portal
    * VITO_LDAP_DN
    * VITO_LDAP_PASSWORD
    * VITO_LDAP_BASE_DN

    NOTE: Even if all of the LDAP credentials are supplied, VITO's remote
    LDAP server will only allow access if the request is coming from one of
    the previously white-listed IP addresses. This means that not every
    machine can communicate with the remote LDAP server.

    """

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

    def authenticate(self, username=None, password=None,
                     password_attributes=None):
        attributes = password_attributes or {}
        vito_password = os.getenv("VITO_SOAP_PASSWORD")
        password_domain = os.getenv("VITO_SOAP_PASSWORD_DOMAIN")
        valid_password = password == vito_password
        valid_domain = attributes.get("domain") == password_domain
        logger.debug("valid_password: {}".format(valid_password))
        logger.debug("valid_domain: {}".format(valid_domain))
        if valid_password and valid_domain:
            UserModel = get_user_model()
            try:
                user = UserModel.objects.get(username=username)
            except UserModel.DoesNotExist:
                logger.debug("Creating a new local user "
                             "for {!r}".format(username))
                user_info = get_user_info(username)
                user = UserModel(username=username,
                                 password="get from VITO LDAP")
                user.is_staff = False
                user.is_superuser = False
                user.email = user_info.get("mail", [""])[0]
                user.first_name = user_info.get("givenName", [""])[0]
                user.last_name = user_info.get("sn", [""])[0]
                user.save()
            result = user
        else:
            result = None
        return result

#class LdapAuthentication(authentication.BasicAuthentication):
#    """Custom authentication for django rest framework that uses LDAP."""
#
#    def authenticate_credentials(self, userid, password):
#        """Authenticate the user against a remote LDAP service.
#
#        Parameters
#        ----------
#        userid: str
#            Id of the django user that is being authenticated. This value
#            is already base64 decoded.
#        password: str
#            LDAP password of the user. This value is already base64 decoded.
#        """
#
#        user = models.
#        auth = None
#        return user, auth
