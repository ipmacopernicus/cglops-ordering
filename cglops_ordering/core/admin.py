import datetime as dt

from django.contrib import admin
import pytz

from . import models


@admin.register(models.FileUpload)
class FileUploadAdmin(admin.ModelAdmin):
    readonly_fields = (
        "expires",
    )
    list_display = (
        "id",
        "is_available",
        "data",
    )

    def is_available(self, instance):
        expiry_date = instance._get_expiration_date()
        now = dt.datetime.now(pytz.utc)
        return now <= expiry_date
    is_available.boolean = True
    is_available.short_description = "Available"


