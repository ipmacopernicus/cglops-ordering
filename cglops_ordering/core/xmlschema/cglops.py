# ./cglops.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:20e73f1909934c07e12c2a2ebbd5b028dee0047b
# Generated 2017-02-15 12:28:25.012338 by PyXB version 1.2.5 using Python 3.4.5.final.0
# Namespace ipma.pt.cglops

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:3efaf8c2-f37a-11e6-a97c-0242ac150005')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.5'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('ipma.pt.cglops', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {ipma.pt.cglops}FileFormatType
class FileFormatType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FileFormatType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 9, 4)
    _Documentation = None
FileFormatType._CF_enumeration = pyxb.binding.facets.CF_enumeration(enum_prefix=None, value_datatype=FileFormatType)
FileFormatType.HDF5 = FileFormatType._CF_enumeration.addEnumeration(unicode_value='HDF5', tag='HDF5')
FileFormatType.NetCDF = FileFormatType._CF_enumeration.addEnumeration(unicode_value='NetCDF', tag='NetCDF')
FileFormatType.GeoTiff = FileFormatType._CF_enumeration.addEnumeration(unicode_value='GeoTiff', tag='GeoTiff')
FileFormatType._InitializeFacetMap(FileFormatType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'FileFormatType', FileFormatType)
_module_typeBindings.FileFormatType = FileFormatType

# Atomic simple type: {ipma.pt.cglops}RescaleValuesType
class RescaleValuesType (pyxb.binding.datatypes.boolean):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RescaleValuesType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 17, 4)
    _Documentation = None
RescaleValuesType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'RescaleValuesType', RescaleValuesType)
_module_typeBindings.RescaleValuesType = RescaleValuesType

# Atomic simple type: {ipma.pt.cglops}MapProjectionType
class MapProjectionType (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MapProjectionType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 22, 4)
    _Documentation = None
MapProjectionType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'MapProjectionType', MapProjectionType)
_module_typeBindings.MapProjectionType = MapProjectionType

# Atomic simple type: {ipma.pt.cglops}BandsType
class BandsType (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BandsType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 33, 4)
    _Documentation = None
BandsType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'BandsType', BandsType)
_module_typeBindings.BandsType = BandsType

# Atomic simple type: {ipma.pt.cglops}DateRangeType
class DateRangeType (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DateRangeType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 38, 4)
    _Documentation = None
DateRangeType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'DateRangeType', DateRangeType)
_module_typeBindings.DateRangeType = DateRangeType

# Complex type {ipma.pt.cglops}RegionOfInterestType with content type ELEMENT_ONLY
class RegionOfInterestType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {ipma.pt.cglops}RegionOfInterestType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RegionOfInterestType')
    _XSDLocation = pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 27, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _HasWildcardElement = True
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RegionOfInterestType = RegionOfInterestType
Namespace.addCategoryObject('typeBinding', 'RegionOfInterestType', RegionOfInterestType)


Format = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Format'), FileFormatType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 8, 4))
Namespace.addCategoryObject('elementBinding', Format.name().localName(), Format)

rescaleValues = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'rescaleValues'), RescaleValuesType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 16, 4))
Namespace.addCategoryObject('elementBinding', rescaleValues.name().localName(), rescaleValues)

MapProjection = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MapProjection'), MapProjectionType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 21, 4))
Namespace.addCategoryObject('elementBinding', MapProjection.name().localName(), MapProjection)

RegionOfInterest = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RegionOfInterest'), RegionOfInterestType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 26, 4))
Namespace.addCategoryObject('elementBinding', RegionOfInterest.name().localName(), RegionOfInterest)

Bands = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Bands'), BandsType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 32, 4))
Namespace.addCategoryObject('elementBinding', Bands.name().localName(), Bands)

DateRange = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DateRange'), DateRangeType, location=pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 37, 4))
Namespace.addCategoryObject('elementBinding', DateRange.name().localName(), DateRange)



def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.WildcardUse(pyxb.binding.content.Wildcard(process_contents=pyxb.binding.content.Wildcard.PC_strict, namespace_constraint=pyxb.binding.content.Wildcard.NC_any), pyxb.utils.utility.Location('/home/user/apps/ordering-giosystem/cglops_ordering/core/xmlschema/ordering_cglops.xsd', 29, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RegionOfInterestType._Automaton = _BuildAutomaton()

