from rest_framework import serializers
from oseoserver import models as oseoserver_models

from . import models


class FileUploadSerializer(serializers.ModelSerializer):
    data = serializers.FileField(use_url=True)
    order_item = serializers.PrimaryKeyRelatedField(
        queryset=oseoserver_models.OrderItem.objects.all()
    )

    class Meta:
        model = models.FileUpload
        fields = (
            "id",
            "created",
            "expires",
            "data",
            "order_item",
        )
        read_only_fields = (
            "id",
            "created",
            "expires",
        )
