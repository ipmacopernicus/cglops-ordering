import logging
import os

from django.conf import settings
from rest_framework.renderers import BaseRenderer
from sendfile import sendfile

from . import models

logger = logging.getLogger(__name__)


class NetcdfRenderer(BaseRenderer):
    media_type = "application/netcdf"
    format = ".nc"
    charset = None
    render_style = "binary"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        instance = models.FileUpload.objects.get(pk=data.get("id"))
        file_path = os.path.join(
            settings.MEDIA_ROOT,
            instance.data.name
        )
        file_name = os.path.basename(file_path)
        result = sendfile(
            request=renderer_context.get("request"),
            filename=file_path,
            attachment=True,
            attachment_filename=os.path.basename(file_path),
            mimetype=self.media_type,
            encoding=self.charset
        )
        response = renderer_context.get("response")
        response["Content-Disposition"] = "attachment; filename={}".format(
            file_path)
        response["Content-length"] = result["Content-length"]
        return result
