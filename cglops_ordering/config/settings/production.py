from .base import *


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": get_environment_variable("DJANGO_DB_NAME"),
        "USER": get_environment_variable("DJANGO_DB_USER"),
        "PASSWORD": get_environment_variable("DJANGO_DB_PASSWORD"),
        "HOST": get_environment_variable("DJANGO_DB_HOST", "127.0.0.1"),
        "PORT": get_environment_variable("DJANGO_DB_PORT", "5432"),
    }
}
