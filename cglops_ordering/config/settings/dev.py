from .base import *

SITE_ID = 2

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": get_environment_variable("DJANGO_DB_NAME"),
        "USER": get_environment_variable("DJANGO_DB_USER"),
        "PASSWORD": get_environment_variable("DJANGO_DB_PASSWORD"),
        "HOST": get_environment_variable("DJANGO_DB_HOST", "127.0.0.1"),
        "PORT": get_environment_variable("DJANGO_DB_PORT", "5432"),
    }
}

LOGGING["handlers"]["console_verbose"] = {
    "class": "logging.StreamHandler",
    "formatter": "verbose",
    "level": "DEBUG",
}

LOGGING["loggers"]["oseoserver"] = {
    "handlers": ["console_verbose",],
    "level": "DEBUG",
}

LOGGING["loggers"]["config"] = {
    "handlers": ["console_verbose",],
    "level": "DEBUG",
}

LOGGING["loggers"]["core"] = {
    "handlers": ["console_verbose",],
    "level": "DEBUG",
}

OSEOSERVER_PRODUCT_ORDER["item_availability_days"] = -10
CELERY_BEAT_SCHEDULE["clean-expired-items"]["schedule"] = 30.0  # seconds
CELERY_BEAT_SCHEDULE["terminate-expired-subscriptions"]["schedule"] = 45.0  # seconds
