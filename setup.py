#!/usr/bin/env python

import io
from os.path import dirname, join

from setuptools import find_packages, setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()

setup(
    name="cglops_ordering",
    version=read("VERSION"),
    description="A django project to process orders for cgl",
    long_description=open("README.md").read(),
    author="Ricardo Garcia Silva",
    author_email="ricardo.silva@ipma.pt",
    license="Apache",
    url="https://gitlab.com/ipmacopernicus/cglops-ordering",
    package_dir={"": "cglops_ordering"},
    packages=find_packages("cglops_ordering"),
    include_package_data=True,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
        ],
    install_requires=[
        "celery",
        "django",
        "oseoserver",
        "psycopg2",
    ],
    zip_safe=False,
)
