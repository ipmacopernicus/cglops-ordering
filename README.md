# cglops-ordering

A `django` and `celery` project to process CGL product orders using the 
OGC OSEO standard.

## Plan

This project aims to be deployable with two different roles:

* OSEO server - Accepting HTTP requests with OSEO orders;
* order processing worker - By taking order requests and processing them 
asynchronously

This means that the installation process needs to be able to differentiate 
between the two roles, as each has specific dependencies and configuration.

Both roles need:

* Access to a `postgresql` database, which will be managed by django's ORM;
* Access to a `rabbitmq` server, which will control queuing of celery tasks;
* Full installation and configuration of a `django` project with the some 
common settings;

The server role will also need:

* The `gunicorn` WSGI server and a standard web server to proxy requests to it;
* The `pyxb` package configured with OGC bindings;
* Some specific settings for `celery-beat` in order to perform routine tasks 
such as monitoring FTP downloads and sending daily notifications to end users;
* An FTP service running on the same host;

## Installation

System wide dependencies:

- `cdo`
- `rabbitmq-server`
- `redis`?

- Install `netcdf-cglops-tools`, which requires `cdo` in the system. It will 
also install `numpy` and `netCDF4` (hopefully using wheels)
- Install `django`
- Install `celery` with `rabbitmq`


## development

- Install stuff
- Compile pyxb OGC bindings


    # start a postgres db container with
    docker run \
      --name <db_container_name> \
      --detach \
      --env POSTGRES_PASSWORD=$DJANGO_DB_PASSWORD \
      -p 5432:5432
      postgres
      
    # start a rabbitmq server container with
    docker run \
      --name <rabbitmq_container_name> \
      --hostname <some_name_for_host> \
      --detach \
      --env RABBITMQ_ERLANG_COOKIE=$RABBITMQ_ERLANG_COOKIE \
      -p $RABBITMQ_PORT:5672 \
      rabbitmq:3-management
      
    # start a redis container with
    docker run \
    --name <redis_container_name> \
    --detach
    redis
    
    # start celery
    celery --app config.celery
    
    # use flower to monitor celery
    flower --app config.celery
    
    
## environment variables

Set the following:

- `DJANGO_SETTINGS_MODULE`
- `DJANGO_EMAIL_HOST`
- `DJANGO_EMAIL_PORT`
- `DJANGO_EMAIL_HOST_USER`
- `DJANGO_EMAIL_HOST_PASSWORD`
- `DJANGO_DB_NAME`
- `DJANGO_DB_USER`
- `DJANGO_DB_PASSWORD`
- `DJANGO_DB_HOST` - Optional (defaults to `localhost`)
- `DJANGO_DB_PORT` - Optional (defaults to `5432`)
- `REDIS_DB` - Optional (defaults to `0`)
- `REDIS_HOST` - Optional (defaults to `localhost`)
- `REDIS_PORT` - Optional (defaults to `6379`)
- `RABBITMQ_ERLANG_COOKIE` - Rabbitmq erlang cookie
- `RABBITMQ_HOST` - Optional (defaults to `localhost`)
- `RABBITMQ_PORT` - Optional (defaults to `5672`)
- `RABBITMQ_USER` - Optional (defaults to `guest`)
- `RABBITMQ_PASSWORD` - Optional (defaults to `guest`)
- `CATALOGUE_URL` - URL of the SW catalogue where product records are stored
- `VITO_LDAP_SERVER` - URL for VITO's LDAP server that is used for authenticating users
- `VITO_LDAP_BASE_DN` - 
- `VITO_LDAP_DN` - 
- `VITO_LDAP_PASSWORD` - 
